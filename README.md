# 基于swing的聊天室

#### 介绍
基于swing实现的聊天室，服务端和客户端；主要功能有，群聊，单聊，添加好友，删除好友，在线离线好友显示，在线好友之间发送文件，聊天记录的查看以及删除。
联系方式qq:981209822

演示视频地址：
【基于swing的java聊天室】 https://www.bilibili.com/video/BV13g411s7LC/?share_source=copy_web&vd_source=1552ff7c22dea81bb03c69a229df013c
#### 软件架构

软件架构说明


#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
